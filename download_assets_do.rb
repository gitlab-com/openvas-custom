require 'net/http'
require 'uri'
require 'json'

bearer = 'Bearer <INSERT TOKEN FOR ENVIRONMENT HERE>'
page = 1
per_page = 200
uri = "https://api.digitalocean.com/v2/droplets?page=#{page}&per_page=#{per_page}"
#req = Net::HTTP::Get.new(url, {'Content-Type' => 'application/json','Authorization' => bearer})
#http = Net::HTTP.new('api.digitalocean.com', 443)
#http.use_ssl = true
next_uri = uri

while !next_uri.nil?
  uri = URI(next_uri)
  req = Net::HTTP::Get.new(uri, {'Content-Type' => 'application/json','Authorization' => bearer})
  
  response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
    http.request(req)
  end
  #puts "Response code: #{response.code}"
  host_hash = JSON.parse(response.body)
  #puts "First host: #{host_hash['droplets'][1]}"
  #puts "Host count: #{host_hash['droplets'].length}\n"
  host_hash['droplets'].each do |host|
    host['networks']['v4'].each do |network|
      next if network['type'] == "private"
      puts "#{network['ip_address']}"
    end
  end
  next_uri = host_hash['links']['pages']['next']
  #puts next_uri
end
