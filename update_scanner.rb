# ruby update_scanner.rb "Scanner Task" "hostnames.txt"

require 'openvas-omp'

@ov = {}
@username = ENV['OV_USER']
@pass = ENV['OV_PASS']

def setup
  @ov=OpenVASOMP::OpenVASOMP.new("user"=>@username,"password"=>@pass)
end

def get_targets
  targets = @ov.target_get_all
end

def get_tasks
  tasks = @ov.task_get_all
end

def find_task_by_name(name)
  tasks = get_tasks
  tasks.each do |t|
    return t["id"] if t["name"] == name
  end
  nil
end

def modify_task(task_id, target_id)
  puts "Modifying task #{task_id} with target: #{target_id}"
  @ov.omp_request_xml("<modify_task task_id=\"#{task_id}\"><target id=\"#{target_id}\"/></modify_task>")  
end

def create_target(name, comment, hosts)
  target_id = @ov.target_create("name" => name, "hosts" => hosts, "comment" => comment)
end

def hosts_from_file(filename)
  hosts = File.open(filename, "r").read
end

def replace_scan_target(task_name, filename)
  date = Time.new.strftime("%Y-%m-%d")

  task_id = find_task_by_name(task_name)
  target_name = "#{task_name} #{date}"
  target_hosts = hosts_from_file(filename)
  target_id = create_target(target_name, target_name, target_hosts)
  modify_task(task_id, target_id)
end

setup
task_name = ARGV[0]
filename = ARGV[1]
replace_scan_target(task_name, filename)
