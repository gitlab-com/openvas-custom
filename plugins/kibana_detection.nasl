###############################################################################
# OpenVAS Vulnerability Test
#
# Elasticsearch Kibana Version Detection
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2
# (or any later version), as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
###############################################################################

if(description)
{
  script_id("30669");
  script_version("$Revision: 6306 $");
  script_tag(name:"cvss_base", value:"10.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:C/I:C/A:C");
  script_tag(name:"last_modification", value:"$Date: 2017-06-11 21:08:57 +0200 (Sun, 11 Jun 2017) $");
  script_tag(name:"creation_date", value:"2016-06-21 12:44:48 +0530 (Tue, 21 Jun 2016)");
  script_name("Kibana without authentication detection");
  script_category(ACT_GATHER_INFO);
  script_family("GitLab");
  script_dependencies("find_service.nasl", "http_version.nasl");
  script_require_ports("Services/www", 9200, 5601);
  script_exclude_keys("Settings/disable_cgi_scanning");
  script_copyright("This script is Copyright (C) 2017 GitLab, B.V.");

  script_tag(name:"summary", value:"Detection of installed version
  of Elasticsearch Kibana that does _not_ require authentication.

  This script sends HTTP GET request and try to ensure the presence of
  Elasticsearch Kibana from the response.");

  script_tag(name:"qod_type", value:"remote_banner");

  exit(0);
}

include("http_func.inc");
include("http_keepalive.inc");
include("cpe.inc");
include("host_details.inc");

port = get_http_port( default:5601 );

foreach dir( make_list_unique( "/", "/kibana", cgi_dirs( port:port ) ) ) {

  if( dir == "/" ) dir = "";

  url = dir + "/app/kibana";
  res = http_get_cache( item:url, port:port );

  if( res =~ "^HTTP/1\.[01] 200" && ( "kbn-name: kibana" >< res || "<title>Kibana</title>" >< res ) ) {
    report = report_vuln_url(port: port, url: url);
    security_message(port: port, data: report);
    exit(0);
  }
}

exit( 0 );
