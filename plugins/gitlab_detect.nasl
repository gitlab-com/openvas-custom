###############################################################################
# OpenVAS Vulnerability Test
#
# GitLab Detection
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
###############################################################################

if(description)
{
  script_id("30666");
  script_version("$Revision: 2222 $");
  script_tag(name:"last_modification", value:"$Date: 2017-03-31 11:50:39 +0200 (Fri, 31 Mar 2017) $");
  script_tag(name:"creation_date", value:"2017-01-17 13:28:38 +0700 (Tue, 17 Jan 2017)");
  script_tag(name:"cvss_base", value:"0.0");
  script_tag(name:"cvss_base_vector", value:"AV:N/AC:L/Au:N/C:N/I:N/A:N");
  script_copyright("This script is Copyright (C) 2017 GitLab, B.V.");

  script_tag(name:"qod_type", value:"remote_banner");

  script_name("GitLab Detection");

  script_tag(name:"summary", value:"Detection of GitLab CE or EE

The script sends a HTTP connection request to the server and attempts to detect the presence of GitLab.");

  script_category(ACT_GATHER_INFO);
  script_family("Product detection");
  script_dependencies("find_service.nasl", "http_version.nasl");
  script_require_ports("Services/www", 80);
  script_exclude_keys("Settings/disable_cgi_scanning");

  exit(0);
}

include("cpe.inc");
include("host_details.inc");
include("http_func.inc");
include("http_keepalive.inc");

port = get_http_port(default: 80);

dir = "/";
install = dir;
if (dir == "/") dir = "";

res = http_get_cache(port: port, item: dir + "/help");

if ("<title>Help · GitLab</title>">< res) {

  version = "unknown";

  vers = eregmatch(pattern: "<span>([0-9]+\.[0-9]+\.[0-9]+)-[ce]e</span>", string: res);
  if (!isnull(vers[1])) {
    version = vers[1];
    set_kb_item(name: "gitlab/version", value: version);
  }

  set_kb_item(name: "gitlab/installed", value: TRUE);

  cpe = build_cpe(value: version, exp: "^([0-9.]+)", base: "cpe:/a:gitlab:gitlab:");
  if (!cpe)
    cpe = 'cpe:/a:gitlab:gitlab';

  register_product(cpe: cpe, location: install, port: port);

  log_message(data: build_detection_report(app: "GitLab", version: version, install: install, cpe: cpe,
                                           concluded: vers[0]),
              port: port);
  exit(0);
}

exit(0);
