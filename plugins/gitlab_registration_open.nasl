###############################################################################
# OpenVAS Vulnerability Test
#
# GitLab Open Registration
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
###############################################################################

CPE = "cpe:/a:gitlab:gitlab";

if (description)
{
  script_id("30668");
  script_version("$Revision: 1 $");
  script_tag(name: "last_modification", value: "$Date: 2017-03-23 03:17:09 +0100 (Thu, 23 Mar 2017) $");
  script_tag(name: "creation_date", value: "2017-03-22 16:06:50 +0700 (Wed, 22 Mar 2017)");
  script_tag(name:"cvss_base", value:"10.0");
  script_tag(name:"cvss_base_vector", value: "AV:N/AC:L/Au:N/C:C/I:C/A:C");

  script_tag(name: "qod_type", value: "remote_banner");

  script_tag(name: "solution_type", value: "Mitigation");

  script_name("GitLab Registration Open");

  script_category(ACT_GATHER_INFO);
  script_copyright("This script is Copyright (C) 2017 GitLab, B.V.");

  script_family("GitLab");
  script_dependencies("gitlab_detect.nasl");
  script_mandatory_keys("gitlab/installed");

  script_tag(name: "summary", value: "GitLab registration is open.");

  script_tag(name: "vuldetect", value: "Checks the signup page for registration support.");

  script_tag(name: "impact", value: "Open registration could allow access to internal projects.");

  script_tag(name: "solution", value: "Disable registration.");

  script_xref(name: "URL", value: "https://gitlab.com/");

  exit(0);
}

include("host_details.inc");
include("http_func.inc");
include("http_keepalive.inc");

if (!port = get_app_port(cpe: CPE))
  exit(0);

if (!dir = get_app_location(cpe: CPE, port: port))
  exit(0);

if (dir == "/")
  dir = "";

url = dir + "/users/sign_in";

if (http_vuln_check(port: port, url: url, pattern: 'role="tab">Register</a>', check_header: TRUE)) {
  report = report_vuln_url(port: port, url: url);
  security_message(port: port, data: report);
  exit(0);
} 

exit(0);
